from setuptools import setup


setup(
    name="nas_bench_nlp",
    version="0.0.1",
    description="Custom awd rnn",
    author="Nikita Klyuchnikov",
    author_email="nikita.klyuchnikov@skolkovotech.ru",
    packages=[
        "nas_bench_nlp",
    ],
    zip_safe=False,
)
